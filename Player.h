#include <iostream>
#include <string>

using namespace std;

class Player {

	private: 
		string name;
		int roll;
		int mod;

	public:
		Player() {}
		Player(string name, int roll, int mod) {
			this -> name = name;
			this -> roll = roll;
			this -> mod = mod;
		}
		string getName() { return name; }
		void setName(string name) { this -> name = name; }
		int getScore() { return roll + mod; }
		int getRoll() { return roll; }
		void setRoll(int roll) { this -> roll = roll; }
		int getMod() { return mod; }
		void setMod(int mod) { this -> mod = mod; }
		string toString() { 
			return (
				this -> name 
				+ " | " 
				+ to_string(this -> roll)
				+ "(" 
				+ ((this -> mod >= 0) ? "+" : "") 
				+ to_string(this -> mod) + ") = " 
				+ to_string(this -> getScore())
			);
		}

	friend ostream& operator << (ostream& out, Player& a) {
		printf(
			"%16s | %2d(%s%2d) = %d", 
			a.name.c_str(), 
			a.roll, 
			((a.mod >= 0) ? "+" : "-"), 
			abs(a.mod), 
			a.getScore());
		return out;
	}

};