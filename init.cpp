#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "Player.h"

using namespace std;

vector<Player> players;

void sort();

void view() {
	
	system("cls");

	sort();

	if ((int)players.size() > 0) {
		for (int i = (int)players.size() - 1; i >= 0; i--) {
			printf("| %2d | ", i);
			cout << players[i] << endl;
		}
	} else {
		cout << "No players entered" << endl;
	}

}

void dispMenu() {
	system("cls");
	view();
	cout
		<< "------------------------------------------------" << endl
		<< "1. Roll initiative" << endl
		<< "2. Add player" << endl
		<< "3. Roll for player" << endl
		<< "4. Edit player" << endl
		<< "5. Remove player" << endl
		<< "6. Clear initiative rolls" << endl
		<< "7. Exit" << endl
		<< "------------------------------------------------" << endl
		<< "Select an option: ";
}

void rollInit() {

	system("cls");
	int roll;

	if ((int)players.size() > 0) {
		for (int i = 0; i < (int)players.size(); i++) {
			cout << "Roll for " << players[i].getName() << ": ";
			cin >> roll;
			players[i].setRoll(roll);
		}
	} else {
		cout << "No players entered" << endl;
	}

	cin.get();
	view();

}

void clearInit() {
	for (int i = 0; i < (int)players.size(); i++) {
		players[i].setRoll(0);
	}
}

void rollPlayer(int i) {
	int roll;
	cout << "Roll for " << players[i].getName() << ": ";
	cin >> roll;
	cin.get();
	players[i].setRoll(roll);
}

void addPlayer() {

	string name;
	int mod;
	system("cls");

	cout << "Name: ";
	cin >> name;

	cout << "Modifier: ";
	cin >> mod;

	players.push_back(*new Player(name, 0, mod));

}

void remove(int i) {
	players.erase(players.begin() + i);
}

void edit(int i) {
	
	string name;
	int mod;
	system("cls");

	cout 
		<< players[i].getName() 
		<< " | " 
		<< ((players[i].getMod() >= 0) ? "+" : "")
		<< players[i].getMod() << endl;

	cout << "New name: ";
	cin >> name;

	cout << "New modifier: ";
	cin >> mod;

	players[i].setName(name);
	players[i].setMod(mod);

}

void sort() { // lazy sort
	for (int i = 0; i < (int)players.size(); i++) {
		for (int j = 0; j < (int)players.size(); j++) {
			if (players[i].getScore() < players[j].getScore()) {
				swap(players[i], players[j]);
			}
		}
	}
}

void swap(Player& a, Player& b) {
	Player temp = a;
	a = b;
	b = a;
}

void readInFile() {

	ifstream fileIn;
	string next;

	fileIn.open("save.dat");
	if (fileIn.fail()) {
		cout << "Unable to read from save file" << endl;
		cin.get();
		exit(1);
	}

	while(fileIn >> next) {
		string name = next;
		fileIn >> next;
		int mod = stoi(next);
		fileIn >> next;
		int roll = stoi(next);
		players.push_back(*new Player(name, roll, mod));
	}

	fileIn.close();

}

void writeOutFile() {
	
	ofstream fileOut;

	fileOut.open("save.dat", ofstream::trunc);
	if (fileOut.fail()) {
		cout << "Unable to write to save file" << endl;
		cin.get();
		exit(1);
	}

	for (int i = 0; i < (int)players.size(); i++) {
		fileOut 
			<< players[i].getName() << " "
			<< players[i].getMod() << " "
			<< players[i].getRoll() << endl;
	}

	fileOut.close();

}

int main() {

	int choice;
	int subChoice;

	readInFile();

	do {

		dispMenu();
		cin >> choice;
		cin.get();

		switch(choice) {
			case 1:
				rollInit();
				break;
			case 2:
				addPlayer();
				break;
			case 3:
				do {
					view();
					cout
						<< "------------------------"				
						<< "------------------------"
						<< endl
						<< "Select a player to roll for: ";
					cin >> subChoice;
					cin.get();
				} while(subChoice >= (int)players.size());
				rollPlayer(subChoice);
				break;
			case 4:
				do {
					view();
					cout
						<< "------------------------"				
						<< "------------------------"
						<< endl
						<< "Select a player to edit: ";
					cin >> subChoice;
					cin.get();
				} while(subChoice >= (int)players.size());
				edit(subChoice);
				break;
			case 5:
				do {
					view();
					cout
						<< "------------------------"				
						<< "------------------------"
						<< endl
						<< "Select a player to remove: ";
					cin >> subChoice;
					cin.get();
				} while(subChoice >= (int)players.size());
				remove(subChoice);
				break;
			case 6:
				clearInit();
				break;
			case 7:
				break;
			default:
				system("cls");
				cout << "Select a valid option, dumbass" << endl;
				cin.get();
				break;
		}

		cin.clear();
		writeOutFile();

	} while(choice != 7);

	writeOutFile();

	return 0;

}